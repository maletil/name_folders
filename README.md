Crea directorios de nombres posibles con archivos aleatorios dentro.
PHP y bash. (*NIX)

Se ejecuta 'names.php' en cli y crea los directorios en /list.
Puedes introducir el número de directorios a crear de esta forma: ``` php -f names.php n=12 ```
siendo 12 el número de directorios.

Resultado:
```
├── ANATOLIY BOUJAADA SASU
│   ├── file018.bin
│   ├── file021.bin
│   └── file093.bin
├── EBENEZER CADARSO GHAZI
│   └── file039.bin
├── HORACIO JOSE BUÑAY PARRAS
├── LIDA HAYAT UYAGUARI
├── MARIA JENIFER GELO ISIDORO
│   ├── file002.bin
│   ├── file040.bin
│   ├── file062.bin
│   └── file063.bin
└── SUNITA LAVELA CAUNEDO
    ├── file017.bin
    ├── file023.bin
    ├── file036.bin
    ├── file059.bin
    ├── file062.bin
    ├── file077.bin
    ├── file084.bin
    └── file088.bin
```
```
list/SUNITA LAVELA CAUNEDO $ ls -lah
drwxr-xr-x 4,0K ago 28 13:54 .
drwxr-xr-x 4,0K ago 28 13:54 ..
-rw-r--r--  31K ago 28 13:54 file017.bin
-rw-r--r--  32K ago 28 13:54 file023.bin
-rw-r--r-- 4,4K ago 28 13:54 file036.bin
-rw-r--r--  21K ago 28 13:54 file059.bin
-rw-r--r--  19K ago 28 13:54 file062.bin
-rw-r--r-- 5,5K ago 28 13:54 file077.bin
-rw-r--r--  11K ago 28 13:54 file084.bin
-rw-r--r--  30K ago 28 13:54 file088.bin
```
