#! /bin/bash

r=$((1 + RANDOM % 10))
n=0
echo "Random number: $r"
while [ $n -lt $r ]; do
  dd if=/dev/urandom of=file$( printf %03d $((RANDOM % 100)) ).bin bs=1 count=$(( RANDOM + 512 ))
  echo $n
  n=$(($n+1))
done

